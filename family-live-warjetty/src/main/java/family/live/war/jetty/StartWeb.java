package family.live.war.jetty;

public class StartWeb {
	private static EmbeddedServer embeddedServer;

	public static void main(String[] args) {
		// Start web server
		int port = 3000;
		try {
			if (args.length == 0) {
				// 该方式能够在开发时快速启动
				embeddedServer = new EmbeddedServer(port, "../src/main/webapp");
			} else {
				// 传入war包的路径，该方法能够在打包完成后启动该war包
				embeddedServer = new EmbeddedServer(port, true, args[0]);
			}
			embeddedServer.start();
		} catch (Exception e) {
			System.exit(0);
		}
	}
}