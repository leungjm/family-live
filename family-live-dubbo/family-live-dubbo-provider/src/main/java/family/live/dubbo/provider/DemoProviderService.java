package family.live.dubbo.provider;

import family.live.dubbo.api.IDemoService;

public class DemoProviderService implements IDemoService {

	@Override
	public String say(String words) {
		System.out.println("provider recv words:" + words);
		return "provicer ret->" + words;
	}

}
