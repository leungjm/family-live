package family.live.dubbo.provider;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Service;

import family.live.dubbo.api.IScanerDemoService;

@Component
@Service(interfaceClass = IScanerDemoService.class)
public class ScannerDemoService implements IScanerDemoService {

	@Override
	public String hello(String name) {
		System.out.println("consumer hello - " + name);
		return "provider hello - " + name;
	}

}
