package family.live.dubbo.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alibaba.dubbo.container.Main;

public class AnnotationBootstrap {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"classpath:META-INF/spring/dubbo-demo-provider.xml");
		context.start();
		System.out.println("provicer is started.");
		synchronized (AnnotationBootstrap.class) {
			while (true) {
				try {
					Main.class.wait();
				} catch (Throwable e) {
				}
			}
		}
	}
}
