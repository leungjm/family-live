package family.live.dubbo.consumer;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alibaba.dubbo.container.Main;

import family.live.dubbo.consumer.scaner.DubboSuport;

public class ConsumerAnnotationBootstrap {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				"classpath:META-INF/spring/dubbo-demo-consumer.xml");
		context.start();
		DubboSuport ds = (DubboSuport) context.getBean("dubboSuport");
		ds.start();
		synchronized (ConsumerAnnotationBootstrap.class) {
			while (true) {
				try {
					Main.class.wait();
				} catch (Throwable e) {
				}
			}
		}
	}
}
