package family.live.dubbo.consumer.scaner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScanerActionDemo {
	// @Reference(interfaceClass = IScanerDemoService.class)
	// private IScanerDemoService scanerDemoService;
	@Autowired
	private DubboSuport dubboSuport;

	// @PostConstruct
	public void start() {
		dubboSuport.start();
	}
}
