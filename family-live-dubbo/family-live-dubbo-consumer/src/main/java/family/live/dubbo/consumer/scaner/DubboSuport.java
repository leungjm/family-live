package family.live.dubbo.consumer.scaner;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;

import family.live.dubbo.api.IScanerDemoService;

@Component
public class DubboSuport {
	@Reference(interfaceClass = IScanerDemoService.class)
	private IScanerDemoService scanerDemoService;

	public void start() {
		for (int i = 0; i < 10000; i++) {
			String res = scanerDemoService.hello("scaner-" + i);
			System.out.println("res->" + res);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
