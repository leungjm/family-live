package family.live.dubbo.consumer;

import java.text.SimpleDateFormat;
import java.util.Date;

import family.live.dubbo.api.IDemoService;

public class ConsumerAction {
	// @Autowired
	private IDemoService demoService;

	public void setDemoService(IDemoService demoService) {
		this.demoService = demoService;
	}

	public void start() throws InterruptedException {
		for (int i = 0; i < Integer.MAX_VALUE; i++) {
			try {
				String hello = demoService.say("world" + i);
				System.out.println("["
						+ new SimpleDateFormat("HH:mm:ss").format(new Date())
						+ "] " + hello);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Thread.sleep(2000);
		}
	}
}
