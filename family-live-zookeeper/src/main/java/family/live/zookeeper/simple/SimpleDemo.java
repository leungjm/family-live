package family.live.zookeeper.simple;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooDefs.Perms;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.ACL;

public class SimpleDemo {

	String connectString = "120.76.122.103:2181";

	int sessionTimeout = 999999;

	ZooKeeper zk;

	public void createInstance() throws IOException {
		zk = new ZooKeeper(connectString, sessionTimeout, this.watcher);
	}

	public void close() throws InterruptedException {
		zk.close();
	}

	Watcher watcher = new Watcher() {

		@Override
		public void process(WatchedEvent event) {
			System.out.println("watcher-" + event.toString());
		}
	};

	public static void main(String[] args) {
		SimpleDemo demo = new SimpleDemo();
		try {
			demo.createInstance();
			List<ACL> acls = new ArrayList<>();
			acls.add(new ACL(Perms.ALL, Ids.ANYONE_ID_UNSAFE));
			String res = demo.zk.create("/familylive", "simpleDemo".getBytes(),
					acls, CreateMode.EPHEMERAL);
			System.out.println("zk create ->" + res);
			demo.close();
			// stay alive until process is killed or Thread is interrupted
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeeperException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
